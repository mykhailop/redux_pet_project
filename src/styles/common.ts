const buttonCommonStyles = `
	font-size: 1rem;
	padding: 0rem 1rem;
    height: 2rem;
    border-radius: 20px;

    :focus {
        outline: none;
    }

	@media only screen 
      and (min-device-width: 375px) 
      and (max-device-width: 812px) 
      and (-webkit-min-device-pixel-ratio: 3) { 
            font-size: 2rem;
            height: 3.2rem;
    }
`;

export { buttonCommonStyles };

const commonFontSize = `
	font-size: 1rem;

	@media only screen 
      and (min-device-width: 375px) 
      and (max-device-width: 812px) 
      and (-webkit-min-device-pixel-ratio: 3) { 
            font-size: 2rem;
    }
`;

export { commonFontSize };
