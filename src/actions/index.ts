import Config, { promoCodes } from 'utils/config';

import {
    REMOVE_ITEMS_FROM_CART,
    FETCH_DETAILS,
    ADD_ITEM_TO_CART,
    APPLY_PROMO,
    RESET_PROMO,
} from './action_types';

export const fetchDetails = (productId) => {
    const productConfig = Config[productId];

    return {
        type: FETCH_DETAILS,
        productConfig,
    };
};

export const removeItemsFromCart = () => ({
    type: REMOVE_ITEMS_FROM_CART,
});

export const addItemToCard = (item) => ({
    type: ADD_ITEM_TO_CART,
    item,
});

export const applyPromo = (promoCode) => {
    const promo = promoCodes.find(codeEntry => codeEntry.code === promoCode);

    return {
        type: APPLY_PROMO,
        promo,
    };
};

export const resetPromo = (promoCode) => {
    return { type: RESET_PROMO };
};

