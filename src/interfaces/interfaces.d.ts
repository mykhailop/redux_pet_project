export interface ConfigStructure {
    id: number | string;
    category: string;
    name: string;
    description?: string;
    price: number;
    taxes: number;
    opinions: number;
    average_opinion: number;
    shipping: number;
    isClean: boolean;
}

export interface DiscountStructure {
    value: number;
    percent: number;
}

export interface CartItemStructure extends ConfigStructure {
    total: number;
    quantity: number;
}

export interface CartItems {
    items: CartItemStructure[];
    price: number;
    shipping: number;
    taxes: number;
    total: number;
    discount: DiscountStructure;
}