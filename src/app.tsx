import React from 'react';
import { createGlobalStyle } from 'styled-components'
import { store } from './store';
import { Provider } from 'react-redux';

import Content from 'modules/content/content';

const GlobalStyle = createGlobalStyle`
    body {
       font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
        'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
        sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
`;

function App() {
    return (
        <Provider store={store}>
            <GlobalStyle />
            <Content />
        </Provider>
    );
}

export default App;
