import React from 'react';
import styled from 'styled-components';
import { currency } from 'utils/config';
import { CartItemStructure } from 'interfaces/interfaces.d';


const StyledDiv = styled.div`
    display: flex;

    > img {
        width: 110px;
        height: 110px;
    }

    @media only screen 
      and (min-device-width: 375px) 
      and (max-device-width: 812px) 
      and (-webkit-min-device-pixel-ratio: 3) { 
            img {
                width: 250px;
                height: 250px;
            }
    }
`;

const StyledH4 = styled.h4`margin-bottom: 0.2rem;`;

const ItemDetails = ({ config }: { config: CartItemStructure }) => {
    return (
        <StyledDiv>
            <img src={`/img/products/${config?.id}.jpg`} />

            <div>
                <h3>{config?.name}</h3>
                <StyledH4>{config?.price + currency}</StyledH4>
                <div>{`Qty: ${config?.quantity}`}</div>
            </div>

        </StyledDiv>
    );
};

export default React.memo(ItemDetails);
