import React, { useState } from 'react';
import { connect } from 'react-redux'
import styled from 'styled-components'

import CollapsibleSection from 'components/collapsible_section/collapsible_section';
import SummaryRow from 'components/summary_row/summary_row';
import InputComponent from 'components/input/input';
import ButtonComponent from 'components/button/button';

import { withCurrency, getShipping } from 'utils/utils';

import { applyPromo } from '../../actions';
import ItemDetails from './item_details';


const StyledDiv = styled.div`
    width: 60vw;
    min-width: 350px;
    max-width: 850px;
`;

const PromoSection = styled.div`
    display: flex;
`;

const PromoInputContainer = styled.div`
    flex: 1;
    margin-right: 0.5rem;
    display: flex;
`;

function ShoppingCart(props) {
    const [ inputValue, setInputValue ] = useState('');

    if (!props.cartState.items.length) {
        return <h1>The cart is empty</h1>;
    }

    return (
        <StyledDiv>
            <SummaryRow
                label="Subtotal"
                value={withCurrency(props.cartState.price)}
            />
            <SummaryRow
                label="Shipping"
                value={getShipping(props.cartState.shipping)}
            />
            <SummaryRow
                label="Taxes and fees"
                value={withCurrency(props.cartState.taxes)}
            />

            { props.cartState.discount.value ?
                <SummaryRow
                    label={`Discount (${props.cartState.discount.percent}%)`}
                    value={withCurrency(props.cartState.discount.value)}
                />
            : null }

            <hr />

            <SummaryRow
                label="Total"
                value={withCurrency(props.cartState.total)}
                bold
            />

            <CollapsibleSection header="See item details">
                { props.cartState.items.map(currentConfig => (
                    <ItemDetails key={currentConfig.id} config={currentConfig} />
                )) }
            </CollapsibleSection>

            <CollapsibleSection header="Apply promo code">
                <PromoSection>
                    <PromoInputContainer>
                        <InputComponent
                            value={inputValue}
                            placeholder="Promo code"
                            changeHandler={setInputValue}
                        />
                    </PromoInputContainer>

                    <ButtonComponent
                        label="Apply"
                        clickHandler={() => {
                            if (!inputValue) {
                                return;
                            }

                            props.applyPromo(inputValue);
                        }}
                    />

                </PromoSection>
            </CollapsibleSection>
        </StyledDiv>
    );
}

const mapStateToProps = (state) => ({
    cartState: state.cartItems,
});

export default connect(
    mapStateToProps,
    { applyPromo },
)(ShoppingCart);
