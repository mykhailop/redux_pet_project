import React from 'react';
import styled from 'styled-components'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import ProductDetails from 'modules/product_details/product_details';
import ShoppingCart from 'modules/shopping_cart/shopping_cart';

import { commonFontSize } from 'styles/common';


// calc width 100% - menu width
const StyledContent = styled.div`
    display: flex;
    justify-content: center;
    ${commonFontSize}
`;

const PaddingDiv = styled.div`
    padding: 5rem;
    max-width: 700px;
    min-width: 150px;
`;

function Content() {
    return (
        <StyledContent>
            <PaddingDiv>
                <Router>
                    <Switch>
                        <Route exact path="/" component={ProductDetails} />
                        <Route exact path="/details/:id" component={ProductDetails} />
                        <Route exact path="/shopping_cart" component={ShoppingCart} />
                    </Switch>
                </Router>
            </PaddingDiv>
        </StyledContent>
    );
}

export default Content;
