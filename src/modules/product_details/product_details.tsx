import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { isEmpty } from 'lodash';
import { useParams } from 'react-router-dom';
import styled from 'styled-components'

import ButtonComponent from 'components/button/button';
import RateComponent from 'components/rate/rate';

import { addItemToCard, removeItemsFromCart, fetchDetails, resetPromo } from '../../actions';
import { withCurrency } from 'utils/utils';

// calc width 100% - menu width
const StyledDiv = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    > div {
    	padding: 1rem;
    	width: 100%;
    }
`;

const Category = styled.div`
	color: gray;
`;

const Header = styled.h2`
	width: 100%;
`;

const FlexContainer = styled.div`
	display: flex;
	justify-content: space-between;
	width: 100%;
`;

const Description = styled.div`
	line-height: 2rem;
	margin-bottom: 1rem;
`;

const CleanContainer = styled.div`
	color: green;
	font-weight: bold;
`;


function ProductDetails(props) {
	const { id } = useParams();

    useEffect(() => {
        props.fetchDetails(id);
    }, []);

    if (!id) {
        const msg = 'Missing selected product. Try another id';
        console.warn(msg);
        return <h1>{msg}</h1>;
    }

	if (isEmpty(props.productConfig)) {
		return <div>Loading...</div>;
	}

    return (
        <StyledDiv>
            <img width="220" height="220" src={`/img/products/${id}.jpg`} />

            <Category>{props.productConfig?.category}</Category>
            <Header>{props.productConfig?.name}</Header>

            <FlexContainer>
            	<RateComponent average={props.productConfig?.average_opinion} total={props.productConfig?.opinions} />

            	{ props.productConfig?.isClean ? <CleanContainer>Clean</CleanContainer> : null }            	
            </FlexContainer>

            <Description>{props.productConfig?.description}</Description>

            <ButtonComponent
            	label={`Purchase ${withCurrency(props.productConfig.price)}`}
            	clickHandler={() => {
                    // TODO delete removeItemsFromCart here
                    // and add some button to clear cart
                    props.removeItemsFromCart();

                    // reset promo in shopping cart after adding
                    // new element to cart
                    
                    props.addItemToCard(props.productConfig);

                    props.resetPromo();

                    props.history.push(`/shopping_cart`);
                }}
            />
        </StyledDiv>
    );
}

const mapStateToProps = (state) => ({
    productConfig: state.config,
});

export default connect(
    mapStateToProps,
    { removeItemsFromCart, fetchDetails, addItemToCard, resetPromo },
)(ProductDetails);
