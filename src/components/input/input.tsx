import React from 'react';
import styled from 'styled-components';

import { buttonCommonStyles } from 'styles/common';

const StyledInput = styled.input`
    min-width: 10rem;
    border: 1px solid #afa7a7;
    width: 100%;
    ${buttonCommonStyles}

    :hover {
        box-shadow: 1px 2px 4px 0px #afa7a7;
    }
`;

interface InputProps {
    label?: string;
    value?: string;
    placeholder?: string;
    changeHandler?: (value: string) => void;
}

const InputComponent = ({ label, value, placeholder, changeHandler }: InputProps) => {
    return (
        <StyledInput 
            placeholder={placeholder}
            value={value}
            onChange={(event) => {
                changeHandler && changeHandler(event.target.value);
            }}
        />
    );
};

export default React.memo(InputComponent);
