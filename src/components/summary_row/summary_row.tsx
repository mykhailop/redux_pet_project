import React from 'react';
import styled from 'styled-components'


// calc width 100% - menu width
const StyledDiv = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 0.5rem 0;
    color: gray;
    ${({ bold }: { bold?: boolean }) => {
    	return bold ? 'font-weight: bold; color: black; padding: 1rem 0;' : '';
    }}
`;

interface SummaryRowProps {
	label: string;
	value: string;
	bold?: boolean;
}

function SummaryRow({ label, value, bold }: SummaryRowProps) {
    return (
        <StyledDiv bold={bold}>
            <div>{label}</div>
            <div>{value}</div>
        </StyledDiv>
    );
}

export default React.memo(SummaryRow);
