import React from 'react';
import styled from 'styled-components';

import { buttonCommonStyles } from 'styles/common';

const StyledButton = styled.button`
    color: white;
    min-width: 10rem;
    background: #a0275a;
    border: 1px solid #a0275a;
    transition: box-shadow 0.2s ease-in-out;
    font-weight: bold;
    ${buttonCommonStyles}

    :hover {
        cursor: pointer;
        box-shadow: 1px 2px 4px 0px #44202c;
    }
`;

interface ButtonProps {
    label: string;
    clickHandler?: () => void;
}

function Button({ label, clickHandler }: ButtonProps) {
    return (
        <StyledButton onClick={() => clickHandler && clickHandler()}>{label}</StyledButton>
    );
}

export default React.memo(Button);
