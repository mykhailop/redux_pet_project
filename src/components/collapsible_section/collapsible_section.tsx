import React, { useState } from 'react';
import styled from 'styled-components'
import { ArrowIosDownwardOutline, ArrowIosUpwardOutline } from '@styled-icons/evaicons-outline';

// calc width 100% - menu width
const StyledDiv = styled.div`
    padding: 0.5rem 0;
    :hover {
        cursor: pointer;
    }
`;

const StyledHeader = styled.div`
    display: flex;
    justify-content: space-between;
`;

const StyledContent = styled.div`
    padding: 1rem 0;
`;

interface SectionProps {
    header: string;
}

const CollapsibleSection: React.FC<SectionProps> = ({ header, children }) => {
    const [ isVisible, setIsVisible ] = useState(false);

    return (
        <StyledDiv>
            <StyledHeader onClick={() => { setIsVisible(!isVisible); }}>
                <div>{header}</div>

                { isVisible ? <ArrowIosUpwardOutline size="24" /> : <ArrowIosDownwardOutline size="24" /> }
            </StyledHeader>
            <hr />
            { isVisible ? 
                <StyledContent>{children}</StyledContent>
            : null }
            
        </StyledDiv>
    );
};

export default CollapsibleSection;
