import React from 'react';
import styled from 'styled-components';
import { map } from 'lodash';
import { Star as EmptyStar } from '@styled-icons/boxicons-regular';
import { Star, StarHalf } from '@styled-icons/boxicons-solid';

const StyledDiv = styled.div`
    display: flex;
    color: #a0275a;
    align-items: center;
`;

const StyledTotal = styled.div`
    color: gray;
    margin-left: 0.5rem;
`;

const MAX_COUNT = 5;
const IconSize = 22;

const parseRate = (average: number) => {
    const fullCount = parseInt(`${average}`, 10);
    const halfCount = Math.ceil(average - fullCount);
    const emptyCount = MAX_COUNT - fullCount - halfCount;

    return [
        fullCount,
        halfCount,
        emptyCount,
    ];

};

function Rate({ average, total }: { average: number, total: number }) {
    const [ fullCount, halfCount, emptyCount ] = parseRate(average);

    return (
    	<StyledDiv>
            { map(new Array(fullCount), (item, index) => (
                <div key={`full${index}`}><Star size={IconSize} /></div>
            )) }

            { halfCount !== 0 && <div><StarHalf size={IconSize} /></div> }

            { map(new Array(emptyCount), (item, index) => (
                <div key={`full${index}`}><EmptyStar size={IconSize} /></div>
            )) }

            <StyledTotal>{total}</StyledTotal>
        </StyledDiv>
    );
}

export default React.memo(Rate);
