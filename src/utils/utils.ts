import { currency } from './config';
import { CartItemStructure } from 'interfaces/interfaces.d';

const withCurrency = (value: number): string => {
    return value + currency;
};

export { withCurrency };

const getShipping = (value: number): string => {
    return value ? withCurrency(value) : 'Free';
};

export { getShipping };


export const getAsNumber = (value: number) => Number(value.toFixed(2));

const countTotalValue = ({ price, taxes, shipping }: CartItemStructure): number => {
    const totalValue = price + taxes + shipping;

    return getAsNumber(totalValue);
};

export { countTotalValue };


const getDiscount = (value: number, promo: number): number => {
    const discount = value / 100 * promo;

    return getAsNumber(discount);
};

export { getDiscount };


const applyPromoItemsReducer = (memo, item: CartItemStructure) => {
    const discount = getDiscount(item.price, memo.discountPercent);

    const discountValue = getAsNumber(discount * item.quantity + memo.discountValue);
    const total = getAsNumber(memo.total + item.total * item.quantity);

    return { ...memo, total, discountValue };
};

export { applyPromoItemsReducer };
