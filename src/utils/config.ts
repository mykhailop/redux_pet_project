// TODO move in some global config in future or LS
export const currency: string = '$';

// in future will be fetched from server
const productsConfig = {
    // product id
    1: {
        id: 1,
        category: 'Lorem Ipsum',
        name: 'Lorem ipsum dolor sit amet',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum molestie lorem vitae tristique. Proin tempus nec leo auctor faucibus. In fermentum magna augue, vel finibus felis eleifend sit amet. Proin eu lectus magna. Nullam ultrices porttitor pulvinar. Aliquam dictum libero quis dui iaculis dictum. Vivamus aliquam dictum lacus, sed luctus sapien luctus sed.',
        price: 12.99,
        taxes: 2.50,
        opinions: 33,
        average_opinion: 4.2,
        // as for now put shipping in product config
        shipping: 0,
        isClean: true,
    },
    2: {
        id: 2,
        category: 'Addidas Shoes',
        name: 'Lorem ipsum dolor sit amet',
        description: 'Lorem ipsum dolor sit amet, magna augue, vel finibus felis eleifend sit amet. Proin eu lectus magna. Nullam ultrices porttitor pulvinar. Aliquam dictum libero quis dui iaculis dictum. Vivamus aliquam dictum lacus, sed luctus sapien luctus sed.',
        price: 2.99,
        taxes: 1.50,
        opinions: 14,
        average_opinion: 3,
        // as for now put shipping in product config
        shipping: 21,
        isClean: true,
    },
};

const promoCodes = [ {
    discount: 30, // percentage values
    code: 'ROWDY',
}, {
    discount: 10, // percentage values
    code: 'lorem',
} ];

export { promoCodes };

export default productsConfig;
