import { createStore } from 'redux';
import reducers from './reducers';

const storageKey = 'rowdy_project';

// its a quick and simple solution for set cart items to localStorage
// we could use redux-persist in bigger projects
const localState = localStorage.getItem(storageKey)
   ? JSON.parse(localStorage.getItem(storageKey) || '')
   : {};

const store = createStore(reducers, localState);

store.subscribe(() => {
	localStorage.setItem(storageKey, JSON.stringify(store.getState()));
});

export { store };
