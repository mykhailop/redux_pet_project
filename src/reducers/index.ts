import { combineReducers } from 'redux';
import {
    REMOVE_ITEMS_FROM_CART,
    FETCH_DETAILS,
    ADD_ITEM_TO_CART,
    APPLY_PROMO,
    RESET_PROMO,
} from '../actions/action_types';

import { CartItems } from 'interfaces/interfaces.d';

import { countTotalValue, getAsNumber, applyPromoItemsReducer } from 'utils/utils';


export const config = (state = {}, action) => {
    switch (action.type) {
        case FETCH_DETAILS:
            return { ...action.productConfig };

        default:
            return state;
    }
};


const cartInitState: CartItems = {
    items: [],
    price: 0,
    shipping: 0,
    taxes: 0,
    total: 0,
    discount: {
        percent: 0,
        value: 0,
    },
};

export const cartItems = (state: CartItems = cartInitState, action) => {
    switch (action.type) {
        case REMOVE_ITEMS_FROM_CART:
            // also reset discount state and total
            return cartInitState;

        case ADD_ITEM_TO_CART:
            const newItem = {
                ...action.item,
                quantity: 1,
                total: countTotalValue(action.item),
            };

            const getRecalculatedState = (oldState, item) => ({
                price: getAsNumber(oldState.price + item.price),
                taxes: getAsNumber(oldState.taxes + item.taxes),
                total: getAsNumber(oldState.total + item.total),
                shipping: getAsNumber(oldState.shipping + item.shipping),
            });

            const existantValue = state.items.find(entry => entry.id === newItem.id);

            if (existantValue) {
                newItem.quantity = existantValue.quantity + 1;

                const filteredItems = state.items.filter(entry => entry.id !== newItem.id);

                return {
                    ...state,
                    ...getRecalculatedState(state, newItem),
                    items: [ ...filteredItems, newItem ],
                };
            }

            return {
                ...state,
                ...getRecalculatedState(state, newItem),
                items: [ ...state.items, newItem ],
            };

        case APPLY_PROMO:
            if (!action.promo) {
                return state;
            }

            const { discountValue, total } = state.items.reduce(applyPromoItemsReducer, {
                total: 0,
                discountValue: 0,
                discountPercent: action.promo.discount,
            });

            return {
                ...state,
                discount: { percent: action.promo.discount, value: discountValue },
                total: getAsNumber(total - discountValue),
            };

        case RESET_PROMO:
            return {
                ...state,
                discount: cartInitState.discount,
                total: state.items.reduce((memo, item) => {
                    return getAsNumber(memo + item.total * item.quantity);
                }, 0),
            };

        default:
            return state;
    }
};

export default combineReducers({
    config,
    cartItems,
});
