import React from 'react';
import Rate from '../src/components/rate/rate';
import renderer from 'react-test-renderer';

test('Test input component', () => {
    let tree = renderer.create(
        <Rate average={4.5} total={33} />
    ).toJSON();

    expect(tree).toMatchSnapshot();

    tree = renderer.create(
        <Rate average={2} total={12} />
    ).toJSON();

    expect(tree).toMatchSnapshot();
});