import {
    getShipping, getAsNumber, countTotalValue,
    getDiscount, applyPromoItemsReducer,
} from '../src/utils/utils';

import { currency } from '../src/utils/config';

test('Test Utils -> getShipping', () => {
    expect(
        getShipping(0)
    ).toBe('Free');

    expect(
        getShipping(1.23)
    ).toBe(`1.23${currency}`);
});


test('Test Utils -> getAsNumber', () => {
    expect(
        getAsNumber(0)
    ).toBe(0);

    expect(
        getAsNumber(0.1 + 0.2)
    ).toBe(0.3);

    expect(
        getAsNumber(1.2366)
    ).toBe(1.24);

    expect(
        getAsNumber(0.12326)
    ).toBe(0.12);
});

test('Test Utils -> countTotalValue', () => {
    expect(
        countTotalValue({ price: 1, taxes: 0.23, shipping: 0 })
    ).toBe(1.23);

    expect(
        countTotalValue({ price: 11, taxes: 2.28, shipping: 1.98 })
    ).toBe(15.26);

    expect(
        countTotalValue({ price: 100, taxes: 23.5, shipping: 7.76 })
    ).toBe(131.26);
});

test('Test Utils -> getDiscount', () => {
    expect(
        getDiscount(100, 30)
    ).toBe(30);

    expect(
        getDiscount(15.99, 25)
    ).toBe(4);

    expect(
        getDiscount(9.99, 30)
    ).toBe(3);
});

test('Test Utils -> applyPromoItemsReducer', () => {
    const items = [{
        id: 1,
        category: 'Lorem Ipsum',
        name: 'Lorem ipsum dolor sit amet',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum molestie lorem vitae tristique. Proin tempus nec leo auctor faucibus. In fermentum magna augue, vel finibus felis eleifend sit amet. Proin eu lectus magna. Nullam ultrices porttitor pulvinar. Aliquam dictum libero quis dui iaculis dictum. Vivamus aliquam dictum lacus, sed luctus sapien luctus sed.',
        price: 12.99,
        taxes: 2.50,
        opinions: 33,
        average_opinion: 4.2,
        // as for now put shipping in product config
        shipping: 0,
        isClean: true,
        quantity: 3,
        total: countTotalValue({ price: 12.99, taxes: 2.50, shipping: 0 }),
    }, {
        id: 2,
        category: 'Nike Air Jordan',
        name: 'Shoes',
        description: 'Lorem ipsum dolor sit amet, magna augue, vel finibus felis eleifend sit amet. Proin eu lectus magna. Nullam ultrices porttitor pulvinar. Aliquam dictum libero quis dui iaculis dictum. Vivamus aliquam dictum lacus, sed luctus sapien luctus sed.',
        price: 112.99,
        taxes: 9.50,
        opinions: 14,
        average_opinion: 3,
        // as for now put shipping in product config
        shipping: 15,
        isClean: true,
        quantity: 1,
        total: countTotalValue({ price: 112.99, taxes: 9.50, shipping: 15 }),
    }];

    // DISCOUNT 10%
    expect(items.reduce(applyPromoItemsReducer, {
        total: 0,
        discountValue: 0,
        discountPercent: 10,
    })).toEqual({ discountValue: 15.2, total: 183.96, discountPercent: 10 });

    // DISCOUNT 30%
    expect(items.reduce(applyPromoItemsReducer, {
        total: 0,
        discountValue: 0,
        discountPercent: 30,
    })).toEqual({ discountValue: 45.6, total: 183.96, discountPercent: 30 });
});