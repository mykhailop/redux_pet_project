import React from 'react';
import CollapsibleSection from '../src/components/collapsible_section/collapsible_section';
import renderer from 'react-test-renderer';

test('Test input component', () => {
    let tree = renderer.create(
        <CollapsibleSection header="See item details">
            <div>Section content</div>
        </CollapsibleSection>
    ).toJSON();

    expect(tree).toMatchSnapshot();

    tree = renderer.create(
        <CollapsibleSection header="See item details">
            <>
                <div>First item</div>
                <div>Second item</div>
            </>
        </CollapsibleSection>
    ).toJSON();

    expect(tree).toMatchSnapshot();
});